import json
from tika import parser
import time

path = "C:\\Users\\Pavan Bagam\\Desktop\\EMA-MHRA\\MHRA\\http~~www.mhra.gov.uk~home~groups~spcpil~documents~spcpil~con1369372623648.pdf"
parsedPDF = parser.from_file(path)
string = parsedPDF["content"]
string = string.lower()

file_name_index1 = path.rfind('~')
file_name_index2 = path.rfind('.')
if file_name_index2 > file_name_index1:
    file_name = path[file_name_index1+1:file_name_index2]
else:
    file_name = path[file_name_index1+1:]

substr1 = " name of the medicinal product"
substr2 = " qualitative and quantitative composition"
substr3 = " special warnings and precautions for use"
substr4 = " interactions with other medicinal products and other forms of interaction"
substr4_1 = " interaction with other medicinal products and other forms of interaction"
substr5 = " undesirable effects"
substr6 = " overdose"
length = len(string)

index1 = string.find(substr1, 0)
index2 = string.find(substr2, index1)
index3 = string.find(substr3, index2)
index4 = string.find(substr4, index3)
if index4 == -1:
    index4 = string.find(substr4_1, index3)
else:
    pass
index5 = string.find(substr5, index4)
index6 = string.find(substr6, index5)
name_section = string[index1:index2]
drug_labels = name_section

json_file_name = file_name + '.JSON'
name_key = "NAME OF THE MEDICINAL PRODUCT"  # drug_labels[0]
warnings_key = "Special Warnings and Precautions for Use"
effects_key = "Undesired Effects"
timestamp_key = 'Timestamp'
name_value = str(drug_labels)
name_value = name_value[:-3]
warnings_value = str(string[index3:index4])
warnings_value = warnings_value[:-3]
effects_value = str(string[index5:index6])
effects_value = effects_value[:-4]
timestamp_value = time.asctime(time.localtime(time.time()))
json_content = ({'ID': json_file_name, 'label_name': {'key': name_key, 'value': name_value}, 'warnings': {'key': warnings_key, 'value': warnings_value}, 'effects': {'key': effects_key, 'value': effects_value}, 'timestamp': {'key': timestamp_key, 'value': timestamp_value}})
print(json_content)
with open(json_file_name, 'w') as outfile:
    json.dump(json_content, outfile)