import json
from tika import parser
import re
import time

path = "C:\\Users\\Pavan Bagam\\Desktop\\EMA-MHRA\\EMA\\http~~www.ema.europa.eu~docs~en_GB~document_library~EPAR_-_Product_Information~human~004654~WC500224307.pdf"
parsedPDF = parser.from_file(path)
tika_content = parsedPDF["content"]
end_string = '\nANNEX II'
annex_ii_position = tika_content.find(end_string)
string = tika_content[:annex_ii_position]
string = string.lower()

# Find the last part of the PDF File name from the path.
file_name_index1 = path.rfind('~')
file_name_index2 = path.rfind('.')
if file_name_index2 > file_name_index1:
    file_name = path[file_name_index1+1:file_name_index2]
else:
    file_name = path[file_name_index1+1:]
file_index = 1

substr1 = " name of the medicinal product"
substr2 = " qualitative and quantitative composition"
substr3 = " special warnings and precautions for use"
substr4 = " interaction with other medicinal products and other forms of interaction"
substr5 = " undesirable effects"
substr6 = " overdose"
length = len(string)

index0 = 0
while index0 < length:
    index1 = string.find(substr1, index0)
    index2 = string.find(substr2, index1)
    index3 = string.find(substr3, index2)
    index4 = string.find(substr4, index3)
    index5 = string.find(substr5, index4)
    index6 = string.find(substr6, index5)
    name_section = string[index1:index2]
    obj = re.search(r'\s*(.*)\s*(.*)\s*(.*)\s*(.*)\s*(.*)\s*(.*)\s*(.*)\s*(.*)\s*(.*)\s*(.*)\s*(.*)\s*(.*)\s*(.*)\s*(.*)\s*(.*)', name_section, re.M)
    drug_labels = [obj.group(2), obj.group(3), obj.group(4), obj.group(5), obj.group(6), obj.group(7), obj.group(8), obj.group(9), obj.group(10), obj.group(11), obj.group(12), obj.group(13), obj.group(14)]
    count = 0
    for i in drug_labels:
        if i:
            count += 1
    for i in range(count-1):
        json_file_name = file_name + '_'
        json_file_name += str(file_index)
        file_index += 1
        json_file_nm = json_file_name + '.JSON'
        name_key = "NAME OF THE MEDICINAL PRODUCT"  # drug_labels[0]
        warnings_key = "Special Warnings and Precautions for Use"
        effects_key = "Undesired Effects"
        timestamp_key = "Timestamp"
        name_value = str(drug_labels[i])
        name_value = name_value[:-1]
        warnings_value = str(string[index3:index4])
        warnings_value = warnings_value[:-3]
        effects_value = str(string[index5:index6])
        effects_value =effects_value[-3]
        timestamp_value = time.asctime(time.localtime(time.time()))
        json_content = ({'ID': json_file_name, 'label_name': {'key': name_key, 'value': name_value}, 'warnings': {'key': warnings_key, 'value': warnings_value}, 'effects': {'key': effects_key, 'value': effects_value},  'timestamp': {'key': timestamp_key, 'value': timestamp_value}})
        print(json_content)
        with open(json_file_nm, 'w') as outfile:
            json.dump(json_content, outfile)

    if index1 == -1:
        break
    else:
        index0 = index6